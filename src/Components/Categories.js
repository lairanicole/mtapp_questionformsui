import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';


class Categories extends Component {
  constructor(){
    super();
    this.state = {
      currentCategory: "",
      isRemoved: false,
    };
  }
  componentDidMount(){
    console.log(this.props.clicks);
    this.setState({

      currentCategory: this.props.categories[0],
      id: this.props.clicks,
     },
    function(){
      this.props.addCategories(this.state.currentCategory, this.state.id, this.state.isRemoved);
      // console.log(this.state.currentCategory);
    });
  }
  selectCategory(e){
    // let arr = [];
    // arr = this.props.selectedCategories;
    // // console.log(e.target.value);
    // arr.push(e.target.value);
    this.setState({
      currentCategory: e.target.value,
    }, function(){
      this.props.addCategories(this.state.currentCategory, this.state.id, this.state.isRemoved);
      console.log(this.state.currentCategory);
    });
  }

  // componentDidMount(){
  //   // this.setState({ currentCategory: this.props.categories[0] });
  //   // console.log(this.props.categories[0]);
  // }

  // componentWillUnmount(){
  //   this.state.isRemoved === false;
  // }
  remove(e){
    this.setState({ isRemoved: true }, function(){
      this.props.addCategories(this.state.currentCategory, this.state.id, this.state.isRemoved);
    });
    e.preventDefault();
  }
  render() {
    if(this.state.isRemoved){
      return null;
    }
    let categories = this.props.categories.map(category => {
      return <option key={category} value={category}>{category}</option>
    });
    // console.log(this.state.newQuestion.selectedCategories);
    return(
      <div>
        <select className="custom-select col-md-2" onChange={this.selectCategory.bind(this)} value={this.state.currentCategory}>
          {categories}
        </select>
        <button className="btn btn-select" onClick={this.remove.bind(this)}>-</button>
      </div>
    );
  }
}

export default Categories;
