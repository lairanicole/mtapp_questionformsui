import React, { Component } from 'react';
import Question from './Question'
import 'bootstrap/dist/css/bootstrap.min.css';


class QuestionList extends Component {
  constructor(){
    super();
    this.state={
      questions: [],
    };
  }

  componentDidMount(){
    this.setState({ questions: this.props.questions },function(){
      this.props.removeQuestion(this.state.questions);
    });
  }
  handleRemoveQuestion(question, isRemoved){
    if(isRemoved){
      let questions=this.state.questions;
      questions[question.key-1]=null
      this.setState({ questions: questions }, function(){
        this.props.removeQuestion(this.state.questions);
        console.log(this.state.questions);
      });
    }
    else{
      // this.setState({ questions: { isRemoved: false } }, function(){
        this.props.removeQuestion(this.state.questions);
      // });
    }
  }
  render() {
    let questionList;
    if(this.state.questions){
      questionList=this.state.questions.filter(question => question != null).map(question => {
        return (<Question key={question.question} question={question} removeQuestion={this.handleRemoveQuestion.bind(this)}/>);
      });
    }
    return (
      <div>
        <strong>{questionList}</strong>
      </div>
    );
  }
}

export default QuestionList;
