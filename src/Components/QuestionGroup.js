import React, { Component } from 'react';
import QuestionList from './QuestionList'
import AddQuestion from './AddQuestion'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link, Route, Switch, Redirect, NavLink } from 'react-router-dom';
import {Collapse} from 'react-collapse';


class QuestionGroup extends Component {
  constructor(props){
    super(props);
    this.state = {
      testName: "",
      questions: [],
      forms: [],
      questionCount: 0,
      api: "Loading...",
      isRemoved: false,
      redirect: null,
      testIsEntered: false,
      testIsOpen: true,
      formsAreOpen: false,
    };
    // this.handleClick.bind(this);
  }
  static defaultProps = {
    mapCategories: {
      "Addition": 1,
      "Subtraction": 2,
      "Multiplication": 3,
      "Division": 4,
    },
    gradeLevel: [1,2,3,4,5,6,7,8,9,10,11,12],
    testType: ["Finals","Elimination","Practice","Team"],
  }
  componentDidMount(){
    fetch('https://mtapp.ml/api/questions/', {mode:'cors'})
    .then(response => response.json())
    .then(data => {
      // console.log(data)
      let qs = data.map(q => {
        return <div key={q.id} >{q.problem_statement}</div>
      })
      this.setState({ api: qs });
      // console.log(this.state.questions);
    })
    .catch(error => console.log('fail',error))
  }

  // componentWillMount(){
  //   this.setState({
  //     questions:[
  //       {
  //         question: "test",
  //         choices: ['A','B','C','D'],
  //         answer: "answer1",
  //         gradeLevel: "",
  //         categories: [],
  //         timeLimit: "",
  //         equation: "",
  //         solution: "",
  //         tips: "",
  //       },
  //       {
  //         question: "test2",
  //         choices: ['A','B','C','D'],
  //         answer: "answer2",
  //         gradeLevel: "",
  //         categories: [],
  //         timeLimit: "",
  //         equation: "",
  //         solution: "",
  //         tips: "",
  //       },
  //     ],
  //     forms: [<AddQuestion key={this.state.questionCount} addQuestion={this.handleAddQuestion.bind(this)}/>],
  //   });
  // }

  handleAddQuestion(question){
    // console.log(question);
    // console.log(isRemoved);
    let cnt=this.state.questionCount;
    let newQuestions = this.state.questions;
    let forms = this.state.forms;
    question.key=cnt;
    // if(isRemoved){
    //   forms[question.id]=null;
    //   console.log(question);
    // }
    // else{
      newQuestions.push(question);
    // }
    this.setState({ questions: newQuestions, questionCount: cnt });
  }
  handleClick(){
    let forms = this.state.forms;
    let cnt = this.state.questionCount;
    forms.push(<AddQuestion key={cnt} testName={this.props.testName} isQuestion={false} questionCount={this.state.questionCount} addQuestion={this.handleAddQuestion.bind(this)}/>);
    cnt++;
    this.setState({
      forms: forms,
      questionCount: cnt,
    });
  }
  handleSubmit(e){
    // this.setState({
    //   questions: this.state.questions,
    // },function(){
    let questionLength = 0;
    for(let i=0;i<this.state.questions.length;i++){
      if(this.state.questions[i]!=null || this.state.questions[i]!=undefined){
        questionLength++;
      }
    }
    if(questionLength==0){
      alert("No questions");
      e.preventDefault();
    }
    else{
      // submit to API

      // make test
      let testID;
      fetch('https://mtapp.ml/api/test/', {
        method: 'POST',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          test_name: this.refs.testName.value,
          grade_level: this.refs.gradeLevel.value,
          test_type: this.refs.testType.value[0],
        })
      })
      .then(response => response.json())
      .then(data => testID=data.id)
      .catch(error => console.log(error));

      // connect test and questions
      // POST
      this.state.questions.map(question =>{
        let url = 'https://mtapp.ml/api/questions/';
        let options = {
                    method: 'POST',
                    mode: 'cors',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        problem_statement: question.question,
                        choice_a: question.choiceA,
                        choice_b: question.choiceB,
                        choice_c: question.choiceC,
                        choice_d: question.choiceD,
                        answer: question.answer,
                        grade_level: question.gradeLevel,
                        time_limit: question.timeLimit,
                        equation: question.equation,
                        solution: question.solution,
                        points: question.points,
                    })
                };
        let response = fetch(url, options)
        .then(response => response.json())
        .then(data => {
          console.log(data);
          for(let i=0;i<question.categories.length;i++){
            fetch('https://mtapp.ml/api/question-subject/', {
              method: 'POST',
              mode: 'cors',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                question: data.id,
                subject: this.props.mapCategories[question.categories[i]],
              })
            })
            .then(response => response.json())
            .then(data => console.log(data))
            .catch(error => console.log(error));
          }

          console.log(data);
          fetch('https://mtapp.ml/api/test-question/', {
            method: 'POST',
            mode: 'cors',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              is_current_question: true,
              test: testID,
              question: data.id,
            })
          })
          .then(response=>response.json())
          .then(data=>console.log(data))
          .catch(error=>console.log(error));
        })
        .catch(error => console.log(error))

        let responseOK = response && response.ok;
        if (responseOK) {
            let data = response.json();
            // do something with data
            // console.log(data[data.length-1]);
        }
      })

      // PUT

      // just connect

      // redirect
      this.setState({ questions: this.state.questions }, function(){
        this.props.addTest(this.state.questions);
      });
    }
  }
  handleRemoveQuestion(questions){
    this.setState({ questions: questions },function(){
      console.log(this.state.questions);
    });
  }

  handleEnterTest(e){
    this.setState({
      testIsEntered: true,
      testName: this.refs.testName.value,
      testIsOpen: false,
      formsAreOpen: true,
    });
    e.preventDefault();
  }
  redirect(e){
    this.setState({ redirect: <Redirect to="/my-tests"/>});
  }

  render() {
    // if(!this.props.testName && !this.props.isQuestion){ // unsafe reload of form sets user back to home page
    //   return <Redirect to="/"/>
    // }

    let forms = this.state.forms.map(form => {
      return form
    });

    let redirect=null;
    if(this.state.redirect){
      redirect=this.state.redirect;
    }

    let allForms=null;
    if(this.state.testIsEntered){
      allForms = (
        <div>
        <div className="border shadow p-3 mb-5 bg-white rounded">
        <h2>{this.state.testName} Questions</h2>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <QuestionList questions={this.state.questions} removeQuestion={this.handleRemoveQuestion.bind(this)}/><br/>
          <input className="btn btn-primary" type="submit" value="Submit" />
        </form><br/>
        </div>


        <div>
          {forms}
        </div>
        <NavLink to="/choose-questions">Choose From Pool</NavLink>

        <button className="btn btn-success" onClick={this.handleClick.bind(this)}>Make Question</button>
        </div>
      )
    }
    let gradeLevel = this.props.gradeLevel.map(grade => {
      return <option key={grade} value={grade}>{grade}</option>
    });
    let testType = this.props.testType.map(test => {
      return <option key={test} value={test}>{test}</option>
    });

    // let consoleLog = console.log(this.props.getKey);
    return (
      <div>
        {redirect}
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-white border">
            <li onClick={this.redirect.bind(this)} className="breadcrumb-item"><a href="">My Tests</a></li>
            <li className="breadcrumb-item active" aria-current="page">Create Test</li>
          </ol>
        </nav>

        <Collapse isOpened={this.state.testIsOpen}>
        <div className="border shadow p-3 mb-5 bg-white rounded">
          <form onSubmit={this.handleEnterTest.bind(this)}>

            <div className="row">
            <label className="col-2">Grade Level</label>
            <select className="custom-select col-md-1" id="grade" ref="gradeLevel">
              {gradeLevel}
            </select>

            <label className="col-2">Test Type</label>
            <select className="custom-select col-md-2" id="testType" ref="testType">
              {testType}
            </select>
            </div><br/>

            <div className="row">
            <label htmlFor="testName" className="col-2">Enter test name</label>
            <input id="testName" className="form-control col-3" type="text" ref="testName"/>
            <input className="btn btn-primary" type="submit" value="Submit" />
            </div>
          </form>

        </div>

      </Collapse>
      {allForms}
      </div>
    );
  }
}

export default QuestionGroup;
