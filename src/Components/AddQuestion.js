import React, { Component } from 'react';
import Categories from './Categories'
import AddQuestionToTest from './AddQuestionToTest'
import {Motion, spring} from 'react-motion';
import {Collapse} from 'react-collapse';
import {UnmountClosed} from 'react-collapse';
import 'bootstrap/dist/css/bootstrap.min.css';

class AddQuestion extends Component {
  constructor(props){
    super(props);
    this.state={
      newQuestion: {
        selectedCategories: [],
        id: this.props.questionCount,
      },
      categories: ['Algebra','Trigo','Geometry'],
      choiceA: "",
      choiceB: "",
      choiceC: "",
      choiceD: "",
      gradeLevel: 1,
      maxCategories: 3,
      clicks: 0,
      more: [],
      customTime: false,
      showForm: true,
      currentAnswer: this.props.choices[0],
      currentGrade: this.props.gradeLevel[0],
      currentTime: this.props.timeLimit[0],
      currentCategory: ['Algebra'],
      isRemoved: false,
    };
    this.handleChange = this.handleChange.bind(this)
    this.handleClick = this.handleClick.bind(this)
    // this.addDropdown = this.addDropdown.bind(this)
  }
  componentDidMount(){
    fetch('https://mtapp.ml/api/subject', {mode: 'cors'})
    .then(response => response.json())
    .then(data => {
      console.log(data);
      let categories = data.map(lastNa => {return lastNa.category_name})
      this.setState({ categories: categories });
      // console.log(categories);
    })
    .catch(error => console.log(error))
  }

  static defaultProps = {
    choices: ['A','B','C','D'],
    gradeLevel: [1,2,3,4,5,6,7,8,9,10,11,12],
    timeLimit: [15,30,60],
  }


  handleSubmit(e){
    let cats = {};
    cats=this.state.newQuestion.selectedCategories.slice(1);
    let sum=0;
    for(let i=0;i<=this.state.newQuestion.selectedCategories.length;i++){
      if(cats[i]){
        sum++;
      }
    }
    console.log(cats);
    e.preventDefault();
    if(!this.refs.question.value || !this.refs.answer.value || !this.refs.gradeLevel.value || sum==0 || !this.refs.timeLimit.value){
      alert('Please fill all required fields');
    } else {
      this.setState({
        newQuestion:{
          testName: this.props.testName,
          question: this.refs.question.value,
          choiceA: this.refs.choiceA.value,
          choiceB: this.refs.choiceB.value,
          choiceC: this.refs.choiceC.value,
          choiceD: this.refs.choiceD.value,
          answer: this.refs.answer.value,
          gradeLevel: this.refs.gradeLevel.value,
          categories: cats,
          timeLimit: Number(this.refs.timeLimit.value),
          equation: this.refs.equation.value,
          solution: this.refs.solution.value,
          tips: this.refs.tips.value,
          points: Number(this.refs.points.value),
        },
        showForm: false,
      },
      function(){
        this.props.addQuestion(this.state.newQuestion);
      });
      e.preventDefault();
    }
  }

  handleChange(e){
    // console.log(e.target.value);
    if(e.target.value == 9){
      this.setState({
        gradeLevel: e.target.value,
        maxCategories: 1,
      });
    }
    else if(e.target.value == 10){
      this.setState({
        gradeLevel: e.target.value,
        maxCategories: 1,
      });
    }
    else if(e.target.value == 11){
      this.setState({
        gradeLevel: e.target.value,
        maxCategories: 1,
      });
    }
    else if(e.target.value == 12){
      this.setState({
        gradeLevel: e.target.value,
        maxCategories: 1,
      });
    }
    else{
      this.setState({
        gradeLevel: e.target.value,
        maxCategories: 3,
      });
    }
    this.setState({ clicks: 0,
      currentGrade: e.target.value });
  }
  // remove: clicks--, newQ.selectedC.pop,
  handleCategories(category, id, isRemoved){
    // let currentCategories = this.state.newQuestion.selectedCategories;
    // currentCategories.push(category);
    let currentCategories = [];
    if(isRemoved){
      let clicks = this.state.clicks;
      clicks--;
      currentCategories = this.state.newQuestion.selectedCategories;
      let displayDropdowns = this.state.more;
      // displayDropdowns.splice(id,1);
      // currentCategories.splice(id, 1);
      // displayDropdowns[id]=null;
      // currentCategories[id]=null;
      delete currentCategories[id];
      delete displayDropdowns[id];

      this.setState({
        newQuestion: {
          selectedCategories: currentCategories,
        },
        clicks: clicks,
        more: displayDropdowns,
      },function(){
        console.log(this.state.clicks);
        console.log(this.state.newQuestion.selectedCategories);
      });
    }
    else{
      currentCategories = this.state.newQuestion.selectedCategories;
      currentCategories[id]=category;
      this.setState({
        newQuestion:{
          selectedCategories: currentCategories,
        },
      }, function(){
        console.log(this.state.newQuestion.selectedCategories);
      });
    }
  }

  handleClick(e){
    let cnt=this.state.clicks;
    cnt++;
    console.log(cnt);
    if(cnt==1){ // new grade selected
      this.setState({
        newQuestion: {
          selectedCategories: [],
        },
        clicks: cnt,
      }, function(){
        if(cnt<=this.state.maxCategories){
          let temp = [];
          temp[cnt]=<Categories key={cnt} addCategories={this.handleCategories.bind(this)} categories={this.state.categories} clicks={this.state.clicks} />;
          this.setState({ more: temp });
        }
      });
    }
    else{
      if(cnt<=this.state.maxCategories){
        this.setState({ clicks: cnt },
        function() {
          let temp = this.state.more;
          let x;
          for(let i=1;i<=this.state.maxCategories;i++){
            if(!temp[i]){
              x=i;
              break;
            }
          }
          temp[x]=<Categories addCategories={this.handleCategories.bind(this)} categories={this.state.categories} clicks={x} />;
          this.setState({ more: temp });
        });
      }
      else{
        this.setState({ clicks: cnt-1 });
        alert("You can only have a maximum of " + this.state.maxCategories + " categories for grade " + this.refs.gradeLevel.value + ".");
      }
    }
    e.preventDefault();
  }
  customTime(e){
    this.setState({ customTime: !this.state.customTime })
    e.preventDefault();
  }
  changeAnswer(e){
    this.setState({ currentAnswer: e.target.value });
  }
  removeForm(e){
    this.setState({ showForm: false });
    e.preventDefault();
  }

  render() {
    let choices = this.props.choices.map(choice => {
      return <option key={choice} value={choice}>{choice}</option>
    });
    let gradeLevel = this.props.gradeLevel.map(grade => {
      return <option key={grade} value={grade}>{grade}</option>
    });
    let categories = this.state.categories.map(category => {
      return <option key={category} value={category}>{category}</option>
    });
    let timeLimit = this.props.timeLimit.map(time => {
      return <option key={time} value={time}>{time}</option>
    });
    let dropdowns = this.state.more
    .map(dropdown => {
      return dropdown;
    });
    console.log(dropdowns);
    if(this.state.clicks == 0){
      for(let i=0;i<1;i++){
        dropdowns = null;
      }
    }
    let addTime = null;
    if(this.state.customTime){
      addTime = <div><label htmlFor="custom">Custom Time Limit (seconds)</label><input className="form-control col-md-1" id="custom" type="number" min="15" max="300" name="timeLimit"/></div>
    }
    let deleteCurrentForm = null;
    if(this.state.showForm){
      deleteCurrentForm =(
        <div className="border shadow p-3 mb-5 bg-white rounded">
        <form onSubmit={this.handleSubmit.bind(this)}>
          <h3> Question Form
          <button className="btn btn-danger float-right" onClick={this.removeForm.bind(this)}>x</button></h3>
          <div className='form-group'>
            <label htmlFor="question">Question</label>
            <input className="form-control" type="text" id="question" ref="question"/>

            <small className="form-text text-muted">Optional: Upload additional question information as an image</small>
            <input className="input-group-text" type="file" multiple accept = 'image/*'/>
          </div>

          <div className="form-group">
            <label>Choices</label><br/>
            <label>A</label><input className="form-control" type="text" ref="choiceA"/><br/>
            <label>B</label><input className="form-control" type="text" ref="choiceB"/><br/>
            <label>C</label><input className="form-control" type="text" ref="choiceC"/><br/>
            <label>D</label><input className="form-control" type="text" ref="choiceD"/>
          </div>

          <div className="form-group row">
            <label htmlFor="answer" className="col-2">Answer</label>
            <select className="custom-select col-md-1" id="answer" onChange={this.changeAnswer.bind(this)} ref="answer" value={this.state.currentAnswer}>
              {choices}
            </select>
          </div>

          <div className="form-group row">
            <label htmlFor="grade" className="col-2">Grade Level</label>
            <select className="custom-select col-md-1" id="grade" ref="gradeLevel" onChange={this.handleChange} value={this.state.currentGrade}>
              {gradeLevel}
            </select>
          </div>

          <div className="form-group">
            <div className="row"><label className="col-md-2" htmlFor="categories">Categories</label>
            <button id="categories" className="btn btn-select" onClick={this.handleClick.bind(this)}>+</button>
            </div><br/>
            {dropdowns}
          </div>

          <div className="form-group">
            <div className="row"><label htmlFor="points" className="col-md-2">Points</label>
            <input className="form-control col-1" type="number" name="points" id="points" ref="points"/></div>
          </div>

          <div className="form-group">
            <div className="row">
              <label className="col-md-2" htmlFor="time">Time Limit (seconds)</label>
              {!this.state.customTime ? (
                <select id="time" className="custom-select col-md-1"ref="timeLimit">
                  {timeLimit}
                </select>
                ) :
                (
                <select id="time" className="custom-select col-md-1" disabled>
                  {timeLimit}
                </select>
              )}
            </div>
            <button className="btn btn-select" onClick={this.customTime.bind(this)}>{!this.state.customTime ? 'Add custom time limit' : 'Remove custom time limit'}</button><br/>
            {addTime}
          </div>

          <div className="border shadow-none p-3 mb-5 bg-light rounded">
          <h4>Optional</h4>

          <div className="form-group">
            <label htmlFor="equation">Equation</label>
            <textarea name="equation" cols="10" rows="5" className="form-control" id="equation" ref="equation"></textarea>
            <small className="form-text text-muted">Optional: Upload additional equation information  as an image</small>
            <input className="input-group-text" type="file" multiple accept = 'image/*'/>
          </div>

          <div className="form-group">
            <label htmlFor="solution">Solution</label>
            <textarea name="solution" cols="10" rows="5" className="form-control" id="solution" ref="solution"></textarea>
            <small className="form-text text-muted">Optional: Upload additional solution information as an image</small>
            <input className="input-group-text" type="file" multiple accept = 'image/*'/>
          </div>

          <div className="form-group">
            <label htmlFor="tips">Tips/Help Links</label>
            <textarea name="tips" cols="10" rows="5" className="form-control" id="tips" ref="tips"></textarea>
          </div>
          </div>

          {this.props.isQuestion ? (
            <input className="btn btn-primary" type="submit" value="Add Question"/>
          ) : (
            <input className="btn btn-primary" type="submit" value="Add to Test"/>
          )}
        </form>
        </div>
    );
    }
    return (
      <div>
        {deleteCurrentForm}
      </div>
    );
  }
}

export default AddQuestion;
