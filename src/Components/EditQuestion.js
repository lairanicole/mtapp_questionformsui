import React, { Component } from 'react';
import Categories from './Categories'
import 'bootstrap/dist/css/bootstrap.min.css';

class EditQuestion extends Component {
  constructor(props){
    super(props);
    this.state={
      categories: ['Algebra','Trigo','Geometry'],
      gradeLevel: 1,
      maxCategories: 3,
      clicks: 0,
      more: [],
      customTime: false,
      showForm: true,
      currentAnswer: this.props.choices[0],
      currentGrade: this.props.gradeLevel[0],
      currentTime: this.props.timeLimit[0],
      currentCategory: ['Algebra'],
      isRemoved: false,
      question: [],
    };
  }
  static defaultProps = {
    choices: ['A','B','C','D'],
    gradeLevel: [1,2,3,4,5,6,7,8,9,10,11,12],
    timeLimit: [15,30,60],
  }
  componentDidMount(){
    let url = 'https://mtapp.ml/api/questions/' + this.props.match.params.id + '/';
    fetch(url, {mode:'cors'})
    .then(response => response.json())
    .then(data => {
      this.setState({ question: data },function(){
        console.log(this.state.questions);
      });
      console.log(data);
    })
    .catch(error => console.log('fail',error))
  }

  handleSubmit(e){
    // let cats = {};
    // cats=this.state.newQuestion.selectedCategories.slice(1);
    // let sum=0;
    // for(let i=0;i<=this.state.newQuestion.selectedCategories.length;i++){
    //   if(cats[i]){
    //     sum++;
    //   }
    // }
    // console.log(cats);
    e.preventDefault();
    if(!this.refs.question.value || !this.refs.answer.value || !this.refs.gradeLevel.value || !this.refs.timeLimit.value){
      alert('Please fill all required fields');
    } else {
      let url='https://mtapp.ml/api/questions/' + this.props.match.params.id + '/';
      fetch(url, {
                  method: 'PUT',
                  mode: 'cors',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                    problem_statement: this.refs.question.value,
                    // image_question: this.refs.image_question.value,
                    choice_a: this.refs.choiceA.value,
                    choice_b: this.refs.choiceB.value,
                    choice_c: this.refs.choiceC.value,
                    choice_d: this.refs.choiceD.value,
                    answer: this.refs.answer.value,
                    grade_level: this.refs.gradeLevel.value,
                    // categories: cats,
                    time_limit: Number(this.refs.timeLimit.value),
                    equation: this.refs.equation.value,
                    // image_equation: this.refs.image_equation.value,
                    solution: this.refs.solution.value,
                    // image_solution: this.refs.image_solution.value,
                    points: Number(this.refs.points.value),
                  })
              })
      .then(response => response.json())
      .then(data => console.log(data))
      .catch(error => console.log(error));
      e.preventDefault();
    }
  }
  handleChange(e){
    // console.log(e.target.value);
    if(e.target.value == 9){
      this.setState({
        gradeLevel: e.target.value,
        maxCategories: 1,
      });
    }
    else if(e.target.value == 10){
      this.setState({
        gradeLevel: e.target.value,
        maxCategories: 1,
      });
    }
    else if(e.target.value == 11){
      this.setState({
        gradeLevel: e.target.value,
        maxCategories: 1,
      });
    }
    else if(e.target.value == 12){
      this.setState({
        gradeLevel: e.target.value,
        maxCategories: 1,
      });
    }
    else{
      this.setState({
        gradeLevel: e.target.value,
        maxCategories: 3,
      });
    }
    this.setState({ clicks: 0,
      currentGrade: e.target.value });
  }
  // remove: clicks--, newQ.selectedC.pop,
  handleCategories(category, id, isRemoved){
    // let currentCategories = this.state.newQuestion.selectedCategories;
    // currentCategories.push(category);
    let currentCategories = [];
    if(isRemoved){
      let clicks = this.state.clicks;
      clicks--;
      currentCategories = this.state.newQuestion.selectedCategories;
      let displayDropdowns = this.state.more;
      // displayDropdowns.splice(id,1);
      // currentCategories.splice(id, 1);
      // displayDropdowns[id]=null;
      // currentCategories[id]=null;
      delete currentCategories[id];
      delete displayDropdowns[id];

      this.setState({
        newQuestion: {
          selectedCategories: currentCategories,
        },
        clicks: clicks,
        more: displayDropdowns,
      },function(){
        console.log(this.state.clicks);
        console.log(this.state.newQuestion.selectedCategories);
      });
    }
    else{
      currentCategories = this.state.newQuestion.selectedCategories;
      currentCategories[id]=category;
      this.setState({
        newQuestion:{
          selectedCategories: currentCategories,
        },
      }, function(){
        console.log(this.state.newQuestion.selectedCategories);
      });
    }
  }

  handleClick(e){
    let cnt=this.state.clicks;
    cnt++;
    console.log(cnt);
    if(cnt==1){ // new grade selected
      this.setState({
        newQuestion: {
          selectedCategories: [],
        },
        clicks: cnt,
      }, function(){
        if(cnt<=this.state.maxCategories){
          let temp = [];
          temp[cnt]=<Categories key={cnt} addCategories={this.handleCategories.bind(this)} categories={this.state.categories} clicks={this.state.clicks} />;
          this.setState({ more: temp });
        }
      });
    }
    else{
      if(cnt<=this.state.maxCategories){
        this.setState({ clicks: cnt },
        function() {
          let temp = this.state.more;
          let x;
          for(let i=1;i<=this.state.maxCategories;i++){
            if(!temp[i]){
              x=i;
              break;
            }
          }
          temp[x]=<Categories addCategories={this.handleCategories.bind(this)} categories={this.state.categories} clicks={x} />;
          this.setState({ more: temp });
        });
      }
      else{
        this.setState({ clicks: cnt-1 });
        alert("You can only have a maximum of " + this.state.maxCategories + " categories for grade " + this.refs.gradeLevel.value + ".");
      }
    }
    e.preventDefault();
  }
  customTime(e){
    this.setState({ customTime: !this.state.customTime })
    e.preventDefault();
  }
  changeAnswer(e){
    this.setState({ currentAnswer: e.target.value });
  }
  removeForm(e){
    this.setState({ isRemoved: true },function(){
      this.props.addQuestion(this.state.newQuestion);
    });
    e.preventDefault();
  }
  render() {
    let choices = this.props.choices.map(choice => {
      return <option key={choice} value={choice}>{choice}</option>
    });
    let gradeLevel = this.props.gradeLevel.map(grade => {
      return <option key={grade} value={grade}>{grade}</option>
    });
    let categories = this.state.categories.map(category => {
      return <option key={category} value={category}>{category}</option>
    });
    let timeLimit = this.props.timeLimit.map(time => {
      return <option key={time} value={time}>{time}</option>
    });
    let dropdowns = this.state.more
    .map(dropdown => {
      return dropdown;
    });
    console.log(dropdowns);
    if(this.state.clicks == 0){
      for(let i=0;i<1;i++){
        dropdowns = null;
      }
    }
    let addTime = null;
    if(this.state.customTime){
      addTime = <div><label htmlFor="custom">Custom Time Limit (seconds)</label><input className="form-control col-md-1" id="custom" type="number" min="15" max="300" name="timeLimit"/></div>
    }
    return (
      <div className="border shadow-none p-3 mb-5 bg-light rounded">
        <h4>Edit {this.state.question.problem_statement}</h4>
        <form onSubmit={this.handleSubmit.bind(this)}>
        <div className='form-group'>
          <label htmlFor="question">Question</label>
          <input className="form-control" type="text" id="question" ref="question" defaultValue={this.state.question.problem_statement}/>

          <small className="form-text text-muted">Optional: Upload additional question information as an image</small>
          <input className="input-group-text" type="file" multiple accept = 'image/*' ref="image_question" defaultValue={this.state.question.image_question}/>
        </div>

        <div className="form-group">
          <label>Choices</label><br/>
          <label>A</label><input className="form-control" type="text" ref="choiceA" defaultValue={this.state.question.choice_a}/><br/>
          <label>B</label><input className="form-control" type="text" ref="choiceB" defaultValue={this.state.question.choice_b}/><br/>
          <label>C</label><input className="form-control" type="text" ref="choiceC" defaultValue={this.state.question.choice_c}/><br/>
          <label>D</label><input className="form-control" type="text" ref="choiceD" defaultValue={this.state.question.choice_d}/>
        </div>

        <div className="form-group">
          <label htmlFor="answer">Answer</label>
          <select className="custom-select" id="answer" onChange={this.changeAnswer.bind(this)} ref="answer" value={this.state.currentAnswer}>
            {choices}
          </select>
        </div>

        <div className="form-group">
          <label htmlFor="grade">Grade Level</label>
          <select className="custom-select" id="grade" ref="gradeLevel" onChange={this.handleChange.bind(this)} value={this.state.currentGrade}>
            {gradeLevel}
          </select>
        </div>

        <div className="form-group">
          <div className="row"><label className="col-md-1" htmlFor="categories">Categories</label>
          <button id="categories" className="btn btn-select" onClick={this.handleClick.bind(this)}>+</button>
          </div><br/>
          {dropdowns}
        </div>

        <div className="form-group">
          <div className="row"><label htmlFor="points" className="col-md-1">Points</label>
          <input className="form-control col-1" type="number" defaultValue={this.state.question.points} name="points" id="points" ref="points"/></div>
        </div>

        <div className="form-group">
          <div className="row">
            <label className="col-md-1" htmlFor="time">Time Limit (seconds)</label>
            {!this.state.customTime ? (
              <select id="time" className="custom-select col-md-1"ref="timeLimit" defaultValue={this.state.question.time_limit}>
                {timeLimit}
              </select>
              ) :
              (
              <select id="time" className="custom-select col-md-1" disabled>
                {timeLimit}
              </select>
            )}
          </div>
          <button className="btn btn-select" onClick={this.customTime.bind(this)}>{!this.state.customTime ? 'Add custom time limit' : 'Remove custom time limit'}</button><br/>
          {addTime}
        </div>

        <div className="border shadow-none p-3 mb-5 bg-light rounded">
        <h4>Optional</h4>

        <div className="form-group">
          <label htmlFor="equation">Equation</label>
          <input type="text" className="form-control" id="equation" ref="equation" defaultValue={this.state.question.equation}/>
          <small className="form-text text-muted">Optional: Upload additional equation information  as an image</small>
          <input className="input-group-text" type="file" ref="image_equation" multiple accept = 'image/*' defaultValue={this.state.question.image_equation}/>
        </div>

        <div className="form-group">
          <label htmlFor="solution">Solution</label>
          <input id="solution" type="text" className="form-control" ref="solution" defaultValue={this.state.question.solution}/>
          <small className="form-text text-muted">Optional: Upload additional solution information as an image</small>
          <input className="input-group-text" type="file" ref="image_solution" multiple accept = 'image/*' defaultValue={this.state.question.image_solution}/>
        </div>

        <div className="form-group">
          <label htmlFor="tips">Tips/Help Links</label>
          <input id="tips" type="text" className="form-control" ref="tips" defaultValue={this.state.question.tips}/>
        </div>
        </div>

        <input className="btn btn-primary" type="submit" value="Update Question"/>
        </form>
      </div>
    );
  }
}

export default EditQuestion;
