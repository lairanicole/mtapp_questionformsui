import React, { Component } from 'react';
import {EditQuestion} from './EditQuestion'
import { Redirect, Link } from 'react-router-dom';
import {MdDelete} from 'react-icons/lib/md';
import {loading} from '../logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';

class UsedInWhichTests extends Component {
  constructor(props){
    super(props);
    this.state={
      testAttributes: [],
      question: "",
      abbrev: "",
    };
  }
  componentDidMount(){
    let containedInTests = {};
    let tests = [];
    let arr=[];
    fetch('https://mtapp.ml/api/questions/'+this.props.match.params.id+'/', {mode: 'cors'})
    .then(response => response.json())
    .then(data => this.setState({question: data.problem_statement}))
    .catch(error => console.log(error));

    fetch('https://mtapp.ml/api/test-question/', {mode: 'cors'})
    .then(response => response.json())
    .then(data => {
      arr=data;
      for(let i=0;i<arr.length;i++){
        if(arr[i].question==this.props.match.params.id){
          if(!containedInTests[arr[i].test]){
            containedInTests[arr[i].test]=true;

            fetch('https://mtapp.ml/api/test/'+arr[i].test+'/', {mode: 'cors'})
            .then(response => response.json())
            .then(data => {
              console.log(data);
              let d=data;
              if(d.test_type=='F'){
                d.test_type="Finals"
              }
              if(d.test_type=='E'){
                d.test_type="Eliminations"
              }
              if(d.test_type=='P'){
                d.test_type="Practice"
              }
              if(d.test_type=='T'){
                d.test_type="Team"
              }
              tests.push(d);
              this.setState({testAttributes: tests});
            })
            .catch(error => console.log(error));
          }
        }
        console.log(tests);
      }
    })
    .catch(error => console.log(error));
  }
  render() {
    let tests = this.state.testAttributes.map(test => {
      return(
        <div className="border-bottom row pb-2 shadow bg-white rounded">
        <div className="col-md-4 pt-2">
          <h3><strong>{test.test_name}</strong></h3>
          <strong>Test Type</strong> {test.test_type} <br/>
          <strong>Grade Level</strong> {test.grade_level}
        </div>
        </div>
      );
    });
    console.log(this.state.testAttributes);
    return (
      <div className="px-3">
      <h1><strong>Tests Containing {this.state.question}</strong></h1>
      {tests}
      </div>
    );
  }
}


export default UsedInWhichTests;
