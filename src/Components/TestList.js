import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import {MdDelete} from 'react-icons/lib/md';
import 'bootstrap/dist/css/bootstrap.min.css';


class TestList extends Component {
  constructor(props){
    super(props);
    this.state={
      tests: [],
      testDates: [],
      searchTerm: "",
      create: null,
      isTest: false, // else question
    };
  }
  componentDidMount(){
    fetch('https://mtapp.ml/api/test/', {mode: 'cors'})
    .then(response => response.json())
    .then(data => {
      // let arr=[];
      // for(let i=0;i<data.length;i++){
      //   arr.push(data[i].test_name);
      // }
      // this.setState({ tests: arr, testDates: data }, console.log(this.state.tests));
      this.setState({ tests: data });
    })
    .catch(error => console.log(error));
  }
  handleChange(){
    this.setState({ searchTerm: this.refs.search.value });
  }

  handleClick(){
    this.setState({ create: <Redirect to="/create-test"/>, isTest: true }, function(){
      this.props.createTest(this.state.isTest);
    });
  }

  deleteTest(test){
    // console.log(test);
    // // fetch api delete test
    // let deleteTest = this.state.tests;
    // let pos=0;
    // for(let i=0;i<deleteTest.length;i++){
    //   if(deleteTest[i]===test){
    //     pos=i;
    //     break;
    //   }
    // }
    // deleteTest.splice(pos, 1);
    // console.log(deleteTest);
    // this.setState({ tests: deleteTest });
  }
  sortAlphabetically(){
    let tests = this.state.tests;
      tests.sort(function(a,b){
        if (String(a.test_name.toLowerCase()) > String(b.test_name.toLowerCase())) return 1;
        if (String(a.test_name.toLowerCase()) < String(b.test_name.toLowerCase())) return -1;
        return 0;
      });
      this.setState({ tests: tests }, console.log(this.state.prevState));
  }
  sortByCreatedDate(){
      let tests = this.state.tests;
      tests.sort(function(a,b){
        return new Date(a.date_created) - new Date(b.date_created)
      });
      this.setState({tests: tests }, console.log(tests));

  }
  sortByUpdatedDate(){
      let tests = this.state.tests;
      tests.sort(function(a,b){
        return new Date(a.date_updated) - new Date(b.date_updated)
      });
      this.setState({tests: tests}, console.log(tests));
  }
  render() {
    if(this.state.create){
      return this.state.create;
    }
    let tests = this.state.tests.filter(test => {
      let j=0;
      for(let i=0;i<test.test_name.length;i++){
        if(this.state.searchTerm.toLowerCase()[j]==test.test_name.toLowerCase()[i]){
          j++;
        }
        else{
          j=0;
        }
        if(j==this.state.searchTerm.length){
          break;
        }
        if(j>=test.test_name.length){
          break;
        }
      }
      if(this.state.searchTerm.length==j){
        return true;
      }
      else{
        return false;
      }
    });
    if(this.state.tests.length === 0){
      tests= <div className="text-center"><h2>Loading...</h2></div>
    } else {
    tests = tests.map(test => {
      return (<div className="border-bottom row pb-3 shadow bg-white rounded">
        <div className="col-md-7 pt-3">
          <h3><strong>{test.test_name}</strong></h3>
          <div className="row">
            <div className="col-md-1 pt-3">
              <Link to={"/edit-test/" + test.id} >Edit</Link>
            </div>
            <div className="col-md-4 pt-3">
              <a style={{cursor: 'pointer'}} className="text-muted">Add Questions</a>
            </div>
            <div className=" pt-3">
              <MdDelete style={{cursor: 'pointer'}}/>
              <a style={{cursor: 'pointer'}} className="text-muted" onClick={this.deleteTest.bind(this, test)}>Delete</a>
            </div>
          </div>
        </div>
        <div className="pull-right row">
        <div className="col pt-3">
          Updated on: {test.date_updated.slice(0, -22)}
        </div>
        <div className="col pt-3">
          <a style={{cursor: 'pointer'}} className="text-muted">Report</a>
        </div>
        </div>
      </div>)
    });
    }
    return (
      <div>
      <div className="row" >
        <div className="col-8" >
          <h1><strong>
            My Tests
          </strong></h1>
        </div>
        <div className="col">
          <form className="row">
            <input className="form-control col-8" onChange={this.handleChange.bind(this)} type="text" placeholder="Search Tests" ref="search"/>
            <input className="btn btn-primary" type="submit" value="Search" />
          </form>
        </div>
      </div>

      <div className="row">
        <div className="col">
          <button onClick={this.handleClick.bind(this)} className="btn btn-primary">+ Create Test</button>
        </div>
      </div><br/>

      <div className="row ml-auto">
        <div className="col-md-9 mr-2">
          {tests}
        </div>
        <div className="stick col-md-2">
          <h4>Sort</h4>
          <form>
          <input name="sort" type="radio" id="update" defaultChecked onChange={this.sortByUpdatedDate.bind(this)} />Date updated<br/>
          <input name="sort" type="radio" id="create" onChange={this.sortByCreatedDate.bind(this)} />Date created<br/>
          <input name="sort" type="radio" id="alpha" onChange={this.sortAlphabetically.bind(this)}/>Alphabetically<br/>
          </form>
        </div>
      </div>

      </div>
    );
  }
}

export default TestList;
