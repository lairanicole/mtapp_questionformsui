import React, { Component } from 'react';
import EditQuestion from './EditQuestion'
import {MdCancel} from 'react-icons/lib/md';
import 'bootstrap/dist/css/bootstrap.min.css';

class Question extends Component {
  constructor(props){
    super(props);
    this.state = {
      editQuestion: {},
      showEditForm: false,
      showQuestion: true,
      hoverQuestion: (<div onMouseEnter={this.handleMouseEnter.bind(this)} onMouseLeave={this.handleMouseLeave.bind(this)}>
        <MdCancel onClick={this.remove.bind(this)} />
        <a href="#" className="col-md-4" onClick={this.handleClick.bind(this)}>{this.props.question.question}</a>
      </div>),
    };
  }
  // handleQuestion(question){
  //   this.setState({ editQuestion: question });
  // }
  componentDidMount(){
    this.props.removeQuestion(this.props.question, !this.state.showQuestion);
  }
  handleClick(e){
    // console.log(this.props.question.question);
    // this.setState({ editQuestion: this.props.question.question });
    this.setState({ showEditForm: !this.state.showEditForm },function(){
      console.log(this.props.question.question)
    });
    // <EditQuestion question={this.props.question} editQuestion={this.handleQuestion.bind(this)} />
    e.preventDefault();
  }
  remove(e){
    this.setState({ showQuestion: false }, function(){
      this.props.removeQuestion(this.props.question, !this.state.showQuestion);
    });
    e.preventDefault();
  }
  handleMouseEnter(e){
    this.setState({ hoverQuestion: (<div onMouseEnter={this.handleMouseEnter.bind(this)} onMouseLeave={this.handleMouseLeave.bind(this)} className="bg-light">
      <MdCancel onClick={this.remove.bind(this)} />
      <a href="#" className="col-md-4" onClick={this.handleClick.bind(this)}>{this.props.question.question}</a>
    </div>) });
  }
  handleMouseLeave(e){
    this.setState({ hoverQuestion: (<div onMouseEnter={this.handleMouseEnter.bind(this)} onMouseLeave={this.handleMouseLeave.bind(this)}>
      <MdCancel onClick={this.remove.bind(this)} />
      <a href="#" className="col-md-4" onClick={this.handleClick.bind(this)}>{this.props.question.question}</a>
    </div>) });
  }
  render() {
    let question=null;
    if(this.state.showQuestion){
      question= this.state.hoverQuestion;
    }
    let editForm = null;
    if(this.state.showEditForm){
      editForm = <EditQuestion question={this.props.question.question}/>;
    }
    return (
      <div>
        {question}
        {editForm}
      </div>
    );
  }
}

export default Question;
