import React, { Component } from 'react';
import Categories from './Categories'
import 'bootstrap/dist/css/bootstrap.min.css';

class EditTest extends Component {
  constructor(props){
    super(props);
    this.state={
      testName: "",
      questions: [],
      questionsFinal: [],
    }
  }
  static defaultProps = {
    choices: ['A','B','C','D'],
    gradeLevel: [1,2,3,4,5,6,7,8,9,10,11,12],
    timeLimit: [15,30,60],
  }
  componentDidMount(){
    fetch('https://mtapp.ml/api/test/' + this.props.match.params.id + '/', {mode: 'cors'})
    .then(response => response.json())
    .then(data => { this.setState({ testName: data.test_name })})
    .catch(error => console.log(error));

    let arr=[];
    fetch('https://mtapp.ml/api/test-question/', {mode: 'cors'})
    .then(response => response.json())
    .then(data => {
      arr=data.slice(0);
      arr.sort(function(a,b){
        return a.test - b.test
      });
      let questions=[];
      for(let i=0;i<data.length;i++){
        // if(arr[i].test > this.props.match.params.id){
        //   break;
        // }
        if(arr[i].test == this.props.match.params.id){
          questions.push(arr[i].question);
        }
      }
      let final=[];
      fetch('https://mtapp.ml/api/questions/'+questions[0]+'/', {mode: 'cors'})
      .then(response => response.json())
      .then(data => {
        final.push(data);
        this.setState({ questions: questions, questionsFinal: final });
      })
      .catch(error=>console.log(error));
    })
    .catch(error => console.log(error));
  }
  selectQuestion(e){
    let questions;
    fetch('https://mtapp.ml/questions/'+e.target.value+'/', {mode: 'cors'})
    .then(response => response.json())
    .then(data => {
      questions = this.state.questionsFinal;
      questions.push(data);
      console.log(questions);
      this.setState({ questionsFinal: questions });
    })
    e.preventDefault();
  }
  render() {
    let questions = this.state.questions.map(question => {
      return(
          <option key={question} value={question}>
            {question}
          </option>
      )
    });

    let selectedQuestion="";
    if(this.state.questionsFinal.length>0){
      selectedQuestion = this.state.questionsFinal[this.state.questionsFinal.length-1];
    }

    let choices = this.props.choices.map(choice => {
      return <option key={choice} value={choice}>{choice}</option>
    });
    let gradeLevel = this.props.gradeLevel.map(grade => {
      return <option key={grade} value={grade}>{grade}</option>
    });

    let timeLimit = this.props.timeLimit.map(time => {
      return <option key={time} value={time}>{time}</option>
    });
    return(
      <div>
        <h3>Edit Test {this.props.match.params.id}</h3>
        <form>
          <label>Test Name</label>
          <input className="form-control col-md-4" type="text" defaultValue={this.state.testName}/>
        </form>
        <br/>

        <h4>Questions</h4>
        <label>Select to Edit</label>
        <select className="custom-select" onChange={this.selectQuestion.bind(this)} id="question" ref="question">
          {questions}
        </select>
        or
        <button className="btn btn-primary">Add Question</button>
        <h2>EDIT FORM HERE</h2>
        <form>
          <label>Question</label>
          <input type="text" className="form-control" defaultValue={selectedQuestion.problem_statement}/><br/>

          <div className="form-group">
            <label>Choices</label><br/>
            <label>A</label><input className="form-control" type="text" ref="choiceA" defaultValue={selectedQuestion.choice_a}/><br/>
            <label>B</label><input className="form-control" type="text" ref="choiceB" defaultValue={selectedQuestion.choice_b}/><br/>
            <label>C</label><input className="form-control" type="text" ref="choiceC" defaultValue={selectedQuestion.choice_c}/><br/>
            <label>D</label><input className="form-control" type="text" ref="choiceD" defaultValue={selectedQuestion.choice_d}/>
          </div><br/>

          <div className="form-group row">
            <label htmlFor="answer" className="col-2">Answer</label>
            <select className="custom-select col-md-1" id="answer" ref="answer" defaultValue={selectedQuestion.answer}>
              {choices}
            </select>
          </div>

          <div className="form-group">
            <label htmlFor="grade">Grade Level</label>
            <select className="custom-select" id="grade" ref="gradeLevel" >
              {gradeLevel}
            </select>
          </div>
        </form>
      </div>
    );
  }
}

export default EditTest;
