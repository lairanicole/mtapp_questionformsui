import React, { Component } from 'react';
import UsedInWhichTests from './UsedInWhichTests'
import {EditQuestion} from './EditQuestion'
import { Redirect, Link } from 'react-router-dom';
import {MdDelete} from 'react-icons/lib/md';
import {loading} from '../logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';

class MyQuestion extends Component {
  constructor(props){
    super(props);
    this.state={
      questions: [],
      searchTerm: "",
      create: null,
      questionSubject: [],
      editLink: null,
    };
  }

  componentDidMount(){
    fetch('https://mtapp.ml/api/questions/', {mode:'cors'})
    .then(response => response.json())
    .then(data => {
      this.setState({ questions: data },function(){
        console.log(this.state.questions);
      });
    })
    .catch(error => console.log('fail',error))
  }
  handleChange(){
    this.setState({ searchTerm: this.refs.search.value });
  }

  // handleClick(){
  //   this.setState({ create: <Redirect to="/create-question"/> }, function(){
  //     this.props.createTest(this.state.isTest);
  //   });
  // }
  deleteQuestion(question){
    // return confirm();
    // if(confirmDelete){
      let url = 'https://mtapp.ml/api/questions/' + question.id + '/';
      console.log(url);
      fetch(url, {
        method: 'DELETE',
        mode: 'cors',
        header: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          id: question.id,
          problem_statement: question.question,
          choice_a: question.choiceA,
          choice_b: question.choiceB,
          choice_c: question.choiceC,
          choice_d: question.choiceD,
          answer: question.answer,
          grade_level: question.gradeLevel,
          time_limit: question.timeLimit,
          equation: question.equation,
          solution: question.solution,
          points: question.points,
        })
      })
      .then(data => console.log(data))
      .catch(error => console.log(error));

      // sort id smallest to biggest

      // binary search the id
      let questions = this.state.questions;
      let mid=Math.floor((questions.length/2));
      let first=0;
      let last=questions.length-1;

      while(first <= mid && last >= mid){
        if(questions[mid].id>question.id){
          last=mid-1;
        }
        else{
          first=mid+1;
        }
        mid=Math.floor((first+last)/2);
        if(questions[mid].id == question.id){
          break;
        }
      }
      questions.splice(mid,1);
      this.setState({ questions: questions });
    // }
  }

  sortAlphabetically(){
    let questions = this.state.questions;
    if(!this.state.tickAlphabetically){
      questions.sort(function(a,b){
        if (String(a.problem_statement.toLowerCase()) > String(b.problem_statement.toLowerCase())) return 1;
        if (String(a.problem_statement.toLowerCase()) < String(b.problem_statement.toLowerCase())) return -1;
        return 0;
      });
      this.setState({ questions: questions }, console.log(this.state.prevState));
    }
  }
  sortByCreatedDate(){
    if(!this.state.tickCreated){
      let questions = this.state.questions;
      questions.sort(function(a,b){
        return new Date(a.date_created) - new Date(b.date_created)
      });
      this.setState({questions: questions }, console.log(questions));
    }
  }
  sortByUpdatedDate(){
    if(!this.state.tickUpdated){
      let questions = this.state.questions;
      questions.sort(function(a,b){
        return new Date(a.date_updated) - new Date(b.date_updated)
      });
      this.setState({questions: questions}, console.log(questions));
    }
  }
  render() {
    // if(this.state.create){
    //   return this.state.create;
    // }
    let questions = this.state.questions.filter(question => {
      let j=0;
      for(let i=0;i<question.problem_statement.length;i++){
        if(this.state.searchTerm.toLowerCase()[j]==question.problem_statement.toLowerCase()[i]){
          j++;
        }
        else{
          j=0;
        }
        if(j==this.state.searchTerm.length){
          break;
        }
        if(j>=question.problem_statement.length){
          break;
        }
      }
      if(this.state.searchTerm.length==j){
        return true;
      }
      else{
        return false;
      }
    });
    if(this.state.questions.length == 0){
      questions = <div className="text-center"><h2>Loading...</h2></div>
    }
    else{
    questions = questions.map(question => {
      return (<div className="border-bottom row pb-2 shadow bg-white rounded">
        <div className="col-md-12 pt-2">
        <div className="row">
          <div className="col-md-4 pt-2">
            <h3><strong>{question.problem_statement}</strong></h3>
          </div>
          <div className="col-md-3 pt-2 ml-auto">
            Updated on: {question.date_updated.slice(0, -22)}
          </div>
          <div className="col-md-2 pt-2">
            <Link to={"/used-in-tests/"+question.id}><a style={{cursor: 'pointer'}} className="text-muted">Used in which tests?</a></Link>
          </div>
          <div className="col-md-1 pt-2 mr-3">
            <a style={{cursor: 'pointer'}} className="text-muted">Report</a>
          </div>
        </div>
          <div className="row">
            <div className="col-md-1">
              <Link to={"/edit-question/" + question.id} >Edit</Link>
            </div>

            <div className="col-md-9">
              <MdDelete style={{cursor: 'pointer'}}/>
              <a style={{cursor: 'pointer'}} className="text-muted" onClick={this.deleteQuestion.bind(this, question)}>Delete</a>
            </div>
          </div>
        </div>
      </div>)
    });}
    return (
      <div>
      <div className="row" >
        <div className="col-8" >
          <h1><strong>
            My Questions
          </strong></h1>
        </div>
        <div className="col">
          <form className="row">
            <input className="form-control col-8" onChange={this.handleChange.bind(this)} type="text" placeholder="Search Questions" ref="search"/>
            <input className="btn btn-primary" type="submit" value="Search" />
          </form>
        </div>
      </div>

      <div className="row">
        <div className="col">
          <Link to="/create-questions"><button className="btn btn-primary">+ Create Question</button></Link>
        </div>
      </div><br/>

      <div className="row ml-auto">
        <div className="col-md-9 mr-2">
          {questions}
        </div>
        <div className="stick col-md-2">
          <h4>Sort</h4>
          <form>
          <input name="sort" type="radio" id="update" defaultChecked onChange={this.sortByUpdatedDate.bind(this)} />Date updated<br/>
          <input name="sort" type="radio" id="create" onChange={this.sortByCreatedDate.bind(this)} />Date created<br/>
          <input name="sort" type="radio" id="alpha" onChange={this.sortAlphabetically.bind(this)}/>Alphabetically<br/>
          </form>
        </div>
      </div>
      </div>
    );
  }
}


export default MyQuestion;
