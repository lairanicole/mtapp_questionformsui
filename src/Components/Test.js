import React, { Component } from 'react';
import QuestionGroup from './QuestionGroup'
import 'bootstrap/dist/css/bootstrap.min.css';


class Test extends Component {
  render() {
    // let testLinks = this.props.tests.map(test => {
    //   return test;
    // });
    let makeTest = null;
    if(this.props.isMade){
      makeTest = (<QuestionGroup testName={this.props.testName} addTest={this.handleQuestionGroup.bind(this)}/>);
    }
    let makeQuestion = null;
    if(this.props.isQuestion){
      makeQuestion = <QuestionGroup isQuestion={this.props.isQuestion} addTest={this.handleQuestions.bind(this)}/>;
    }
    return (
      <div className="container-fluid">
        {makeTest}
        {makeQuestion}
      </div>
    );
  }
}

export default Test;
