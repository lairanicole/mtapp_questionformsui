import React, { Component } from 'react';
import AddQuestion from './AddQuestion'
import QuestionList from './QuestionList'
import { Redirect, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

class MakeQuestions extends Component {
  constructor(props){
    super(props);
    this.state = {
      questions: [],
      forms: [],
      questionCount: 0,
      api: "Loading...",
      isRemoved: false,
      redirect: null,
      testIsEntered: false,
      testIsOpen: true,
      formsAreOpen: false,
      lastPostId: null,
      questionSubject: [],
    };
  }
  static defaultProps = {
    mapCategories: {
      "Addition": 1,
      "Subtraction": 2,
      "Multiplication": 3,
      "Division": 4,
    },
  }
  handleSubmit(e){
    let questionLength = 0;
    for(let i=0;i<this.state.questions.length;i++){
      if(this.state.questions[i]!=null || this.state.questions[i]!=undefined){
        questionLength++;
      }
    }
    if(questionLength==0){
      alert("No questions");
      e.preventDefault();
    }
    else{
      // submit to API
      let id;
      this.state.questions.map(question =>{
        let url = 'https://mtapp.ml/api/questions/';
        let options = {
                    method: 'POST',
                    mode: 'cors',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        problem_statement: question.question,
                        choice_a: question.choiceA,
                        choice_b: question.choiceB,
                        choice_c: question.choiceC,
                        choice_d: question.choiceD,
                        answer: question.answer,
                        grade_level: question.gradeLevel,
                        time_limit: question.timeLimit,
                        equation: question.equation,
                        solution: question.solution,
                        points: question.points,
                    })
                };
        let response = fetch(url, options)
        .then(response => response.json())
        .then(data => {
          console.log(data);
          for(let i=0;i<question.categories.length;i++){
            fetch('https://mtapp.ml/api/question-subject/', {
              method: 'POST',
              mode: 'cors',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                question: data.id,
                subject: this.props.mapCategories[question.categories[i]],
              })
            })
            .then(response => response.json())
            .then(data => console.log(data))
            .catch(error => console.log(error));
          }
        })
        .catch(error => console.log(error))

        let responseOK = response && response.ok;
        if (responseOK) {
            let data = response.json();
            // do something with data
            // console.log(data[data.length-1]);
        }
      })

      this.setState({ questions: this.state.questions, redirect: <Redirect to="/my-questions" /> }, function(){
        console.log(this.state.questionSubject);
      });
    }
  }

  handleClick(){
    let forms = this.state.forms;
    let cnt = this.state.questionCount;
    forms.push(<AddQuestion key={cnt} isQuestion={true} questionCount={this.state.questionCount} addQuestion={this.handleAddQuestion.bind(this)}/>);
    cnt++;
    this.setState({
      forms: forms,
      questionCount: cnt,
    });
  }
  handleAddQuestion(question){
    let cnt=this.state.questionCount;
    let newQuestions = this.state.questions;
    let forms = this.state.forms;
    question.key=cnt;
    newQuestions.push(question);
    this.setState({ questions: newQuestions, questionCount: cnt });
  }
  handleRemoveQuestion(questions){
    this.setState({ questions: questions },function(){
      console.log(this.state.questions);
    });
  }
  redirect(){
    this.setState({ redirect: <Redirect to="/my-questions" />});
  }

  render() {
    if(this.state.redirect){
      return (
        this.state.redirect
      )
    }
    let questionContainer=(
      <div className="border shadow p-3 mb-5 bg-white rounded">
        <h3>Add Questions</h3>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <QuestionList questions={this.state.questions} removeQuestion={this.handleRemoveQuestion.bind(this)}/><br/>
          <input className="btn btn-primary" type="submit" value="Submit" />
        </form>
      </div>
    )

    let forms=(
      <div>
        {this.state.forms}
      </div>
    )
    return (
      <div className="colorPadding">

        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-white border">
            <li onClick={this.redirect.bind(this)} className="breadcrumb-item"><a href="">My Questions</a></li>
            <li className="breadcrumb-item active" aria-current="page">Create Questions</li>
          </ol>
        </nav>

        {questionContainer}
        {forms}
        <button className="btn btn-success" onClick={this.handleClick.bind(this)}>Make Question</button>

      </div>
    );
  }
}


export default MakeQuestions;
