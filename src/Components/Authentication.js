import React, { Component } from 'react';
import {FormGroup, FormControl, ControlLabel} from 'react-bootstrap';


class Authentication extends Component {
	constructor(props){
	super(props);
		this.state = {
      username:"",
      password: "",
	};
}
 handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }
  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true});   
  } 
	render()
	{
		return (
			<div>
				<form>
          			<FormGroup controlId="username">
            			<ControlLabel>Username</ControlLabel>
            			<FormControl value={this.state.username} onChange={this.handleChange} type="text"/>
          			</FormGroup>
          			<FormGroup controlId="password">
           				<ControlLabel>Password</ControlLabel>
            			<FormControl value={this.state.password} onChange={this.handleChange} type="password" />
          			</FormGroup>
                <label>
              Forgot your password?   
            </label>
          <button className="submit" type="submit"> Submit </button>
        	</form>    		 
			</div>
				);
	}
}

export default Authentication;