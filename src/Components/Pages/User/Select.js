import React, { Component } from 'react';
import Register from './Register';
import RegisterCoach from './RegisterCoach';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class Select extends Component {
  
  onClick = () => {
     this.setState({
       value: this.state.value
     });
		}
  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }
  handleSubmit = async event => {
    event.preventDefault();
	}
  
  render() {
    var userType;
    return (
    <Router className = "Header">

    <div>
      <table>
        <div>
        <th>
          <Link to="/register" props={{ userType: "student" }} ><button>Student</button></Link>
        </th>
        <th>
          <Link to="/registerCoach" props={{ userType: "coach" }} ><button > Coach </button></Link>
        </th>
        <th>
          <Link to="/registerParent" props={{ userType: "parent" }} ><button > Parent </button></Link>
        </th>
        </div>
      </table>

      <hr />

      <Route path="/register" component={Register} />
      <Route path="/registerCoach" component={RegisterCoach} />
      <Route path="/registerParent" component={RegisterCoach} />
    </div>
  </Router>
    );
  }
}
export default Select;