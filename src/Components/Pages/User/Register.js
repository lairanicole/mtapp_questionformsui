import React, { Component } from 'react';
import {FormGroup, FormControl, ControlLabel} from 'react-bootstrap';
import fetch from 'isomorphic-fetch';

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userType: this.props.userType,
      isLoading: false,
      type:"student",
      username:"",
      email: "",
      password: "",
      confirmPassword: "",
      grade: null,
      parent: [],
      coach: [],
                  };
  }

  validateForm() {
    return (
      this.state.email.length > 0 && 
      this.state.password.length > 0 && 
      this.state.password === this.state.confirmPassword);
  }
  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }
  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true});   
  }
  submitData() {
    fetch('http://172.104.74.98/api/questions/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      pesonName: 'personName',
      Age: 'Age',
    })
    })
  }
  renderForm(){
    return (
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="username">
            <ControlLabel>Username</ControlLabel>
            <FormControl value={this.state.username} onChange={this.handleChange} type="text"/>
          </FormGroup>
          <FormGroup controlId="email">
            <ControlLabel>Email</ControlLabel>
            <FormControl autoFocus type="email" value={this.state.email} onChange={this.handleChange} />
          </FormGroup>
          <FormGroup controlId="password">
            <ControlLabel>Password</ControlLabel>
            <FormControl value={this.state.password} onChange={this.handleChange} type="password" />
          </FormGroup>
          <FormGroup controlId="confirmPassword">
            <ControlLabel>Confirm Password </ControlLabel>
            <FormControl value={this.state.confirmPassword} onChange={this.handleChange} type="password" />
          </FormGroup>
              <FormGroup controlId = "grade"> 
              <select value={this.state.value} onChange={this.handleChange}>
                  <option value="Kindergarten"> Kindergarten </option>
                  <option value="Grade 1"> Grade 1 </option>
                  <option value="Grade 2"> Grade 2 </option>
                  <option value="Grade 3"> Grade 3 </option>
                  <option value="Grade 4"> Grade 4 </option>
                  <option value="Grade 5"> Grade 5 </option>
                  <option value="Grade 6"> Grade 6 </option>
                  <option value="Grade 7"> Grade 7 </option>
                  <option value="Grade 8"> Grade 8 </option>
                  <option value="Grade 9"> Grade 9 </option>
                  <option value="Grade 10"> Grade 10 </option>
                  <option value="Grade 11"> Grade 11 </option>
                  <option value="Grade 12"> Grade 12 </option>
                </select>
              </FormGroup>
          <button type="submit"> Submit </button> 
        </form> 
        );
}
  render(){
    return (
      <div className ="Register">
        {this.renderForm()}
      </div>
      );
      }
  }
export default Register;