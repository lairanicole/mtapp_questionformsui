import React, { Component } from 'react';
import {FormGroup, FormControl, ControlLabel} from 'react-bootstrap';

class RegisterCoach extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userType: this.props.userType,
      isLoading: false,
      type:"",
      username:"",
      email: "",
      password: "",
      confirmPassword: "",
      student: null,
                  };
  }
  validateForm() {
    return (
      this.state.email.length > 0 && 
      this.state.password.length > 0 && 
      this.state.password === this.state.confirmPassword);
  }
  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }
  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true});   
  }
  
  renderForm(){
    return (
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="username">
            <ControlLabel>Username</ControlLabel>
            <FormControl value={this.state.username} onChange={this.handleChange} type="text"/>
          </FormGroup>
          <FormGroup controlId="email">
            <ControlLabel>Email</ControlLabel>
            <FormControl autoFocus type="email" value={this.state.email} onChange={this.handleChange} />
          </FormGroup>
          <FormGroup controlId="password">
            <ControlLabel>Password</ControlLabel>
            <FormControl value={this.state.password} onChange={this.handleChange} type="password" />
          </FormGroup>
          <FormGroup controlId="confirmPassword">
            <ControlLabel>Confirm Password </ControlLabel>
            <FormControl value={this.state.confirmPassword} onChange={this.handleChange} type="password" />
          </FormGroup>
           <button type="submit"> Submit </button> 
        </form> 
        );
}
  render(){
    return (
      <div className ="RegisterCoach">
        {this.renderForm()}
      </div>
      );
      }
  }
export default RegisterCoach;