import React, { Component } from 'react';
import {FormGroup, FormControl, ControlLabel} from 'react-bootstrap';
import './../Profile/Graphing/Profile.css';

class ProfileView extends Component {

  constructor(props) {
    super(props);
    var currentUser = this.props.userType;

    this.state = {
      userType: this.props.userType,
      isLoading: false,
      type:"",
      username:"",
      email: "",
      password: "",
      confirmPassword: "",
      school: "",
      coach: [""],
      parent: [""],
      student: [""],
                  };
  }

  ComponentDidMount(){
    fetch("http://172.104.74.98/api/questions/")
    .then(results => results.json())
    .then(
      (result) => {
        this.setState({
          isLoaded: true,
          items: result.items
        });
      },
       (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }
  validateForm() {
    return (
      this.state.email.length > 0 && 
      this.state.password.length > 0 && 
      this.state.password === this.state.confirmPassword);
  }
  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }
  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true});   
  }

  renderCoachForm(){
    return (
        <form className="form" onSubmit={this.handleSubmit}>
          <div className="fields">
          <FormGroup controlId="username">
            <ControlLabel>Username</ControlLabel>
            <FormControl value={this.state.username} onChange={this.handleChange} type="text"/>
          </FormGroup>
          <FormGroup controlId="email">
            <ControlLabel>Email</ControlLabel>
            <FormControl autoFocus type="email" value={this.state.email} onChange={this.handleChange} />
          </FormGroup>
          <FormGroup controlId="password">
            <ControlLabel>Password</ControlLabel>
            <FormControl value={this.state.password} onChange={this.handleChange} type="password" />
          </FormGroup>
          <FormGroup controlId="confirmPassword">
            <ControlLabel>Confirm Password </ControlLabel>
            <FormControl value={this.state.confirmPassword} onChange={this.handleChange} type="password" />
          </FormGroup>
          <FormGroup controlId="school">
            <ControlLabel>Username</ControlLabel>
            <FormControl value={this.state.username} onChange={this.handleChange} type="text"/>
          </FormGroup>
          <FormGroup controlId="coach">
            <ControlLabel>Student/s</ControlLabel>
            <FormControl value={this.state.coach} onChange={this.handleChange} type="text"/>
            <button> Add Student </button>
          </FormGroup>
          </div>
          <button type="submit"> Submit </button> 
        </form> 
        );
}
renderStudentForm(){
    return (
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="username">
            <ControlLabel>Username</ControlLabel>
            <FormControl value={this.state.username} onChange={this.handleChange} type="text"/>
          </FormGroup>
          <FormGroup controlId="email">
            <ControlLabel>Email</ControlLabel>
            <FormControl autoFocus type="email" value={this.state.email} onChange={this.handleChange} />
          </FormGroup>
          <FormGroup controlId="password">
            <ControlLabel>Password</ControlLabel>
            <FormControl value={this.state.password} onChange={this.handleChange} type="password" />
          </FormGroup>
          <FormGroup controlId="confirmPassword">
            <ControlLabel>Confirm Password </ControlLabel>
            <FormControl value={this.state.confirmPassword} onChange={this.handleChange} type="password" />
          </FormGroup>
          <FormGroup controlId="school">
            <ControlLabel>Username</ControlLabel>
            <FormControl value={this.state.school} onChange={this.handleChange} type="text"/>
          </FormGroup>
          <FormGroup controlId="student">
            <ControlLabel>Student/s</ControlLabel>
            <FormControl value={this.state.student} onChange={this.handleChange} type="text"/>
            <button> Add Coach </button>
          </FormGroup>
          
          <button type="submit"> Submit </button> 
        </form> 
        );
}
  render(){
//    if (currentUser === "student")
    return (
      <div>
        {this.renderCoachForm()}
      </div>
      );
  //  return (
    //  <div>
      //  {this.renderCoachForm()}
      //</div>
      //);
      }
  }

export default ProfileView;