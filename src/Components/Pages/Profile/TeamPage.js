import React, { Component } from 'react';
import TeamGraph from './Graphing/TeamGraph';
import ScrollArea from 'react-scrollarea';

class TeamPage extends Component {
  constructor() {
    super();
    
    this.state = {
      showMenu: false,
    };
    
    this.showMenu = this.showMenu.bind(this);
    this.closeMenu = this.closeMenu.bind(this);
  }
  
  showMenu(event) {
    event.preventDefault();
    
    this.setState({ showMenu: true }, () => {
      document.addEventListener('click', this.closeMenu);
    });
  }
  
  closeMenu() {
    this.setState({ showMenu: false }, () => {
      document.removeEventListener('click', this.closeMenu);
    });
  }

  render() {
    return (
      <div>
      <table>
      <tr> Team Name: </tr>
      <tr> Team Members: </tr>
      <tr> General Average: </tr>
      <tr> Total Tests Taken: </tr>
      
      <TeamGraph />
      </table>
      </div>
    );
  }
}

export default TeamPage;
