import React, { Component } from 'react';
import StudentGraph from './Graphing/StudentGraph';

class StudentPage extends Component {
  constructor() {
    super();
    
    this.state = {
      showMenu: false,
    };
    
    this.showMenu = this.showMenu.bind(this);
    this.closeMenu = this.closeMenu.bind(this);
  }
  
  showMenu(event) {
    event.preventDefault();
    
    this.setState({ showMenu: true }, () => {
      document.addEventListener('click', this.closeMenu);
    });
  }
  
  closeMenu() {
    this.setState({ showMenu: false }, () => {
      document.removeEventListener('click', this.closeMenu);
    });
  }

  render() {
    return (
      <div>
      <table>
      <tr> Student Name: </tr>
      <th> <StudentGraph /> </th>
      <tr> General Average: </tr>
      <tr> Total Tests Taken: </tr>
      </table>
      </div>
    );
  }
}

export default StudentPage;
