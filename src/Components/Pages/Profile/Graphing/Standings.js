import React, { Component } from "react";
import { ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, LabelList, Label, Tooltip, Legend } from 'recharts';
import './Profile.css';

class Standings extends Component {
constructor(props) {
   super(props);
  
  }
  render() {
    const users = [{x: 1, y: 200}, 
                  {x: 12, y: 100},
                  {x: 12, y: 300},
                  {x: 8, y: 250},
                  {x: 6, y: 400},
                  {x: 9, y: 280},
                  {x: 3, y: 380},
                  {x: 2, y: 160},
                  {x: 5, y: 290},
                  {x: 6, y: 320},
                  {x: 8, y: 110},
                  {x: 9, y: 340},
                  {x: 3, y: 280},
                  {x: 12, y: 265},
                  {x: 11, y: 160},
                  {x: 7, y: 300}
                  ]
        const students = [{x: 1, y: 300}, 
                  {x: 12, y: 200},
                  {x: 8, y: 210},
                  {x: 9, y: 140},
                  {x: 3, y: 380},
                  {x: 12, y: 165},
                  {x: 11, y: 360},
                  {x: 7, y: 200}
                  ]
    return (
    <ScatterChart className="standings" width={1000} height={400} margin={{top: 20, right: 20, bottom: 20, left: 100}}>
        <XAxis type="number" dataKey={'x'} name='level'>
        <Label value="Average Score" offset={0} position="bottom"/>
        </XAxis>
        <YAxis type="number" dataKey={'y'} name='average score'>
        <Label value="Grade Level" offset={0} position="left" />
        </YAxis>
        <CartesianGrid />
        <Legend />
        <Tooltip cursor={{strokeDasharray: '3 3'}}/>
        <Scatter data={users} fill='#8884d8'>
        </Scatter>
      </ScatterChart>
    );
  }
}

export default Standings;