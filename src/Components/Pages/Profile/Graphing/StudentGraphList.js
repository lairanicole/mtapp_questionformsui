import React, { Component } from "react";
import { Label, BarChart, ResponsiveContainer, ReferenceLine, Bar, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import StudentGraph from './StudentGraph';
import './Profile.css';

class StudentGraphList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: this.props.username,
       };
  }

  render() {

    const Laira = [
    {name: 'Laira',
    "YAxis": "", 
    "Average Score": 30,
    "Team": 30,
    "15s Questions": 40,
    "30s Questions": 25,
    "60s Questions": 30,
    "Individual": 35,  
    "Part 1": 25,
    "Part 2": 35,
    "Part 3": 40}
    ];

    const Lance = [
    {name: 'Lance',
    "YAxis": "", 
    "Average Score": 30,
    "Team": 30,
    "15s Questions": 40,
    "30s Questions": 25,
    "60s Questions": 30,
    "Individual": 35,  
    "Part 1": 25,
    "Part 2": 35,
    "Part 3": 40}
    ];

    const Kyle = [
    {name: 'Kyle',
    "YAxis": "", 
    "Average Score": 30,
    "Team": 30,
    "15s Questions": 40,
    "30s Questions": 25,
    "60s Questions": 30,
    "Individual": 35,  
    "Part 1": 25,
    "Part 2": 35,
    "Part 3": 40}
    ];

    
      return (

      <table className="graphList">
        <tr>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Laira} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}>
              <Label value="Elimination" offset={0} position="top" />
              </XAxis>
              <YAxis type="category" dataKey="name" />
              <ReferenceLine isFront={true} x={30} stroke="red" />
              <Tooltip/>
              <Bar dataKey="Average Score" fill="#8884d8"/>
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Laira} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}>
              <Label value="Team" offset={0} position="top" />
              </XAxis>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="Team" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Laira} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}>
              <Label value="15s Questions" offset={0} position="top" />
              </XAxis>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="15s Questions" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Laira} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}>
              <Label value="30s Questions" offset={0} position="top" />
              </XAxis>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="30s Questions" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Laira} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}>
              <Label value="60s Questions" offset={0} position="top" />
              </XAxis>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="60s Questions" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Laira} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}>
              <Label value="Individual" offset={0} position="top" />
              </XAxis>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="Individual" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Laira} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}>
              <Label value="Part 1" offset={0} position="top" />
              </XAxis>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="Part 1" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Laira} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}>
              <Label value="Part 2" offset={0} position="top" />
              </XAxis>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="Part 3" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Laira} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}>
              <Label value="Part 3" offset={0} position="top" />
              </XAxis>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="Part 3" fill="#8884d8" />
            </BarChart>
          </th>
       </tr>

        <tr>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Lance} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]} />
              <YAxis type="category" dataKey="name" />
              <ReferenceLine isFront={true} x={30} stroke="red" />
              <Tooltip/>
              <Bar dataKey="Average Score" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Lance} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="Team" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Lance} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="15s Questions" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Lance} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="30s Questions" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Lance} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="60s Questions" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Lance} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="Individual" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Lance} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="Part 1" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Lance} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="Part 3" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Lance} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="Part 3" fill="#8884d8" />
            </BarChart>
          </th>
       </tr>

        <tr>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Kyle} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]} />
              <YAxis type="category" dataKey="name" />
              <ReferenceLine isFront={true} x={30} stroke="red" />
              <Tooltip/>
              <Bar dataKey="Average Score" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Kyle} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="Team" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Kyle} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="15s Questions" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Kyle} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="30s Questions" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Kyle} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="60s Questions" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Kyle} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="Individual" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Kyle} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="Part 1" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Kyle} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="Part 3" fill="#8884d8" />
            </BarChart>
          </th>
          <th col-span="1">
            <BarChart 
              width={210} 
              height={100} 
              data={Kyle} 
              layout="vertical"
              margin={{top: 20, right: 30, left: 10, bottom: 0}}>
              <CartesianGrid strokeDasharray="3 3"/>
              <XAxis orientation="top" type="number" domain={[0, 50]}/>
              <YAxis type="category" dataKey="YAxis" />
              <Tooltip/>
              <Bar dataKey="Part 3" fill="#8884d8" />
            </BarChart>
          </th>
       </tr>
      </table>
    );
  }
}

export default StudentGraphList;