import React, {Component} from 'react';
import { VictoryZoomContainer, VictoryTheme, VictoryStack, VictoryAxis, VictoryChart, VictoryBar } from 'victory';

class VictoryGraph extends Component {
constructor(props) {
    super(props);

    this.state = {
      username: this.props.username,
       };

  }

   render() {

    const correct = [
    {attempt: 1, value: 20},
    {attempt: 2, value: 30},
    {attempt: 3, value: 40},
    {attempt: 4, value: 15},
    {attempt: 5, value: 35},
    {attempt: 6, value: 45},
    ];

const incorrect = [
    {attempt: 1, value: 20},
    {attempt: 2, value: 30},
    {attempt: 3, value: 40},
    {attempt: 4, value: 15},
    {attempt: 5, value: 35},
    {attempt: 6, value: 45},
    ];

    const late = [
    {attempt: 1, value: 20},
    {attempt: 2, value: 30},
    {attempt: 3, value: 40},
    {attempt: 4, value: 15},
    {attempt: 5, value: 35},
    {attempt: 6, value: 45},
    ];
    
    return (
      <div>

        <VictoryChart
        height={200}
        width={300}
        domainPadding={20}
      >
        <VictoryAxis
          tickValues={[1, 2, 3, 4]}
          tickFormat={["Attempt 1", "Attempt 2", "Attempt 3", "Attempt 4", "Attempt 5", "Attempt 6"]}/>
        <VictoryAxis
          dependentAxis
          tickFormat={(x) => (`$${x / 100}`)}/>
        <VictoryStack>
          <VictoryBar
            data={correct}
            x="attempt"
            y="value"/>
          <VictoryBar
            data={correct}
            x="attempt"
            y="value"/>
            <VictoryBar
            data={correct}
            x="attempt"
            y="value"/>
        </VictoryStack>
      </VictoryChart>
      </div>
    );
  }
}


export default VictoryGraph;