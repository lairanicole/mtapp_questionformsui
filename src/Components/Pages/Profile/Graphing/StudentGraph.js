import React, { Component } from "react";
import { BarChart, ResponsiveContainer, ReferenceLine, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import './Profile.css';

class StudentGraph extends Component {
constructor(props) {
   super(props);
   this.state = {testID: this.props.testID,
      graphType: this.props.graphType,
      partOneCorrect: this.props.partOneCorrect,
      partOneIncorrect: this.props.partOneIncorrect,
      partTwoCorrect: this.props.partTwoCorrect,
      partTwoIncorrect: this.props.partTwoIncorrect,
      partThreeCorrect: this.props.partThreeCorrect,
      partThreeIncorrect: this.props.partThreeIncorrect,
    };
  }
  render() {
    const PartA = [

      {name: 'Attempt 1(7/20)', correct: 30, late: 40, incorrect: 20},
      {name: 'Attempt 2(8/20)', correct: 60, late: 40, incorrect: 30},
      {name: 'Attempt 3(9/20)', correct: 50, late: 40, incorrect: 40},
      {name: 'Attempt 5(9/20)', correct: 70, late: 40, incorrect: 40},
      {name: 'Attempt 6(9/20)', correct: 80, late: 40, incorrect: 40},
      {name: 'Attempt 7(9/20)', correct: 30, late: 40, incorrect: 40},
      {name: 'Attempt 8(9/20)', correct: 40, late: 40, incorrect: 40},
      {name: 'Attempt 9(9/20)', correct: 50, late: 40, incorrect: 40},
      {name: 'Attempt 10(9/20)', correct: 60, late: 40, incorrect: 40},
    ];

    const Elimination = [{score: 45, elimination: 45}]
    return (
    <table>
    
    <tr>
    <label> 15s Questions </label>
     <BarChart      
        height={300}
        width={1200} 
        data={PartA}
        barCategoryGap="50%"
        barSize={200}
        margin={{top: 20, right: 90, left: 20, bottom: 5}}>
       <CartesianGrid strokeDasharray="3 3"/>
       <XAxis dataKey="name"/>
       <YAxis domain={[0, 100]}/>
       <Tooltip/>
       <Legend verticalAlign="top" height={30} />
       <Bar dataKey="correct" stackId="a" fill="#8884d8" />
       <Bar dataKey="late" stackId="a" fill="#32CD32" />
       <Bar dataKey="incorrect" stackId="a" fill="#b72b34" />
      </BarChart>

       <label> 30s Questions </label>
     <BarChart      
        height={300}
        width={1200} 
        data={PartA}
        barCategoryGap="50%"
        barSize={200}
        margin={{top: 20, right: 90, left: 20, bottom: 5}}>
       <CartesianGrid strokeDasharray="3 3"/>
       <XAxis dataKey="name"/>
       <YAxis domain={[0, 100]}/>
       <Tooltip/>
       <Bar dataKey="correct" stackId="a" fill="#8884d8" />
       <Bar dataKey="late" stackId="a" fill="#32CD32" />
       <Bar dataKey="incorrect" stackId="a" fill="#b72b34" />
       </BarChart>

       <label> 60s Questions </label>
     <BarChart      
        height={300}
        width={1200} 
        data={PartA}
        barCategoryGap="50%"
        barSize={200}
        margin={{top: 20, right: 90, left: 20, bottom: 5}}>
       <CartesianGrid strokeDasharray="3 3"/>
       <XAxis dataKey="name"/>
       <YAxis domain={[0, 100]}/>
       <Tooltip/>
       <Bar dataKey="correct" stackId="a" fill="#8884d8" />
       <Bar dataKey="late" stackId="a" fill="#32CD32" />
       <Bar dataKey="incorrect" stackId="a" fill="#b72b34" />
       </BarChart>
    </tr>
    </table>
    );
  }
}

export default StudentGraph;