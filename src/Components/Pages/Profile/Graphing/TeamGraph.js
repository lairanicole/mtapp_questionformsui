import React, { Component } from "react";
import { BarChart, ResponsiveContainer, ReferenceLine, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

class TeamGraph extends Component {
constructor(props) {
    super(props);
   this.state = {
      testID: this.props.testID,
      graphType: this.props.graphType,
      fifteenIncorrect: this.props.fifteenIncorrect,
      fifteenOnTime: this.props.fifteenOnTime,
      fifteenLate: this.props.fifteenLate,
      thirtyIncorrect: this.props.thirtyIncorrect,
      thirtyOnTime: this.props.thirtyOnTime,
      thirtyLate: this.props.thirtyLate,
      sixtyIncorrect: this.props.sixtyIncorrect,
      sixtyOnTime: this.props.sixtyOnTime,
      sixtyLate: this.props.sixtyLate,
      partOneCorrect: this.props.partOneCorrect,
      partOneIncorrect: this.props.partOneIncorrect,
      partTwoCorrect: this.props.partTwoCorrect,
      partTwoIncorrect: this.props.partTwoIncorrect,
      partThreeCorrect: this.props.partThreeCorrect,
      partThreeIncorrect: this.props.partThreeIncorrect,
    };
  }
  render() {
    const data = [
      {name: 'Elimination', timeA: 20, timeB: 32, timeC: 24},
      
    ];

    return (
      <table>
      <th>
      <label>Elimination </label>
      <BarChart 
        width={300} 
        height={400} 
        data={data}
         margin={{top: 20, right: 90, left: 20, bottom: 5}}>
       <CartesianGrid strokeDasharray="3 3"/>
       <XAxis dataKey="name"/>
       <YAxis domain={[0, 100]}/>
       <Tooltip/>
       <Legend />
       <ReferenceLine isFront={true} y={70} stroke="black" label={{ position: 'right',  value: 'Elimination', fill: 'red', fontSize: 12 }} />
       <Bar dataKey="timeA" stackId="a" fill="#8884d8" />
       <Bar dataKey="timeB" stackId="a" fill="#82ca9d" />
       <Bar dataKey="timeC" stackId="a" fill="#82ca00" />
      </BarChart>
    </th>
    </table>
    );
  }
}

export default TeamGraph;