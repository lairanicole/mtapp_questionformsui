import React, {Component} from 'react';
import StudentPage from './StudentPage';
import StudentGraphList from './Graphing/StudentGraphList';
import './Graphing/Profile.css';
import StudentGraph from './Graphing/StudentGraph';

class StudentList extends Component {
constructor(props) {
    super(props);

    this.state = {
      username: this.props.username,
       };
  }

   render() {
   
    return (
      <div>
          <table>
            <tr className="label"> Grade 12 Students (Average Scores)</tr>
            <tr className="graphListContainer">
            <StudentGraphList />
            </tr>
            <tr className="graphContainer">
            <StudentGraph />
            </tr>
          </table>
    	</div>
    );
  }
}


export default StudentList;
