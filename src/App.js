import React, { Component } from 'react';
import QuestionGroup from './Components/QuestionGroup'
import TestList from './Components/TestList'
import MyQuestions from './Components/MyQuestions'
import MakeQuestions from './Components/MakeQuestions'
import EditQuestion from './Components/EditQuestion'
import EditTest from './Components/EditTest'
import AddQuestionToTest from './Components/AddQuestionToTest'
import StudentList from './Components/Pages/Profile/StudentList';
import ProfileView from './Components/Pages/User/ProfileView';
import Standings from './Components/Pages/Profile/Graphing/Standings';
import Select from './Components/Pages/User/Select';
import Authentication from './Components/Authentication';
import UsedInWhichTests from './Components/UsedInWhichTests';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link, Route, Redirect, Switch, withRouter, Prompt } from 'react-router-dom';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      tests: [
      [
        {
          testName: "Test1",
          answer: "A",
          categories: ["Algebra"],
          equation: "",
          gradeLevel: "1",
          key: 1,
          points: "",
          question: "question1",
          solution: "",
          timeLimit: "15",
          tips: "",
        },
        {
          testName: "Test1",
          answer: "A",
          categories: ["Algebra"],
          equation: "",
          gradeLevel: "1",
          key: 1,
          points: "",
          question: "question2",
          solution: "",
          timeLimit: "15",
          tips: "",
        },
        {
          testName: "Test1",
          answer: "A",
          categories: ["Algebra"],
          equation: "",
          gradeLevel: "1",
          key: 1,
          points: "",
          question: "question3",
          solution: "",
          timeLimit: "15",
          tips: "",
        }
      ],
      ],
      isMade: false,
      showForm: true,
      testName: "",
      isLoaded: false,
      items: [],
      isQuestion: false,
      redirect: false,
      redirectQ: false,
      testNames: [],
      isTest: false,
    };
  }
  handleQuestionGroup(test){
    // this.setState({ testName: null });
    console.log(test);
    let tests = this.state.tests;
    tests.push(test);
    this.setState({ tests: tests });
  }

  handleCreateTest(isTest){
    console.log(isTest);
    this.setState({ isTest: true });
  }


  handleQuestions(question){
    let questions = this.state.questions;
    questions.push(question);
    this.setState({ questions: question });
  }
  handleSubmit(e){
    if(!this.state.isMade){
      if(this.refs.testName.value.trim()){
        this.setState({
          isMade: true,
          showForm: false,
          testName: this.refs.testName.value,
          redirect: true,
        });
      }
      else{
        alert("Test can't be blank.");
      }
    }
    e.preventDefault();
  }
  handleQuestionSubmit(e){
    this.setState({
      isQuestion: true,
      showForm: false,
      redirectQ: true,
    });
    e.preventDefault();
  }
  componentDidMount(){
    // let url = 'https://securitypractice.tk/api/student/';
    // let options = {
    //             method: 'POST',
    //             mode: 'cors',
    //             headers: {
    //               'Accept': 'application/json',
    //               'Content-Type': 'application/json',
    //             },
    //             body: JSON.stringify({
    //                 name: 'Sample Student',
    //                 email: 'student@example.com',
    //                 password: 'pass123123',
    //                 grade_level: 1,
    //                 username: 'student1',
    //
    //             })
    //         };
    // let response = fetch(url, options)
    // .then(data => console.log(data))
    // .catch(error => console.log(error));
    // let responseOK = response && response.ok;
    // if (responseOK) {
    //     let data = response.json();
    //     // do something with data
    // }

  }

  render() {
    // let testLinks = this.state.tests.map(test => {
    //   return test;
    // });
    // let makeTest = null;
    // if(this.state.isMade){
    //   makeTest = (<QuestionGroup testName={this.state.testName} addTest={this.handleQuestionGroup.bind(this)}/>);
    // }

    let redirect=null;
    if(this.state.redirect){
      redirect= <Redirect to="/create-test"/>;
    }
    let redirectQ=null;
    if(this.state.redirectQ){
      redirectQ=<Redirect to="/create-questions"/>
    }

    let tests;

    tests = this.state.tests.map(test => {
      return <p>{test[0].testName}</p>;
    });

    let questions;
    questions = this.state.tests[0].map(question => {
      return <p>{question.question}</p>
    });
    // let tests=console.log(this.state.tests[0]);
    // let testForm = null;
    // if(this.state.showForm){
    let testForm = (
        <div className="row">
        <div className="col border-right">
        <form onSubmit={this.handleSubmit.bind(this)}>
          <div className="form-row text-center">
          <label htmlFor="test" className="col-md-3">Create Test</label>
          <input className="form-control col-md-4" id="test" placeholder="Enter test name"type="text" ref="testName"/>
          <input className="btn btn-primary col-md-3 rounded-0" type="submit" value="Create Test" />
          </div>
        </form>
        <div className="col-1">
          <h2>TESTS</h2>
          {tests}
        </div>
        </div>
        <div className="col">
          <Link to="/create-questions"><input className="btn btn-primary rounded-0 col-md-4" value="+ Create Questions"/></Link><br/>
          <h2>MY QUESTIONS</h2>
          {questions}
        </div>
        </div>
      );
      // <form onSubmit={this.handleQuestionSubmit.bind(this)} >
      //   <input className="btn btn-primary rounded-0" type="submit" value="Make Questions"/>
      // </form>
    // }
    // let makeQuestion = null;
    // if(this.state.isQuestion){
    //   makeQuestion = <QuestionGroup isQuestion={this.state.isQuestion} addTest={this.handleQuestions.bind(this)}/>;
    // }
    const Home = ({ match }) =>(
      <div className="container-fluid">
        <TestList createTest={this.handleCreateTest.bind(this)} />
      </div>
    );

    const Test = ({ match }) =>(
      <QuestionGroup addTest={this.handleQuestionGroup.bind(this)} isQuestion={false} />
    );

    const Questions = ({ match }) =>(
      // <QuestionGroup isQuestion={true} addTest={this.handleQuestions.bind(this)}/>
      <MakeQuestions />
    );

    return (
      <div className="all">
      <div className="mx-5" >
        <div className="fixed-top">
      <nav className="navbar navbar-expand-lg navbar-light" style={{backgroundColor: "#005b96"}} >
        <Link to="/"><a className="navbar-brand text-white" href="#">MTAPP</a></Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav">
            <li className="nav-item active">
              <Link to="/my-tests"><a className="nav-link text-white" href=""> My Tests </a></Link>
            </li>
            <li className="nav-item active">
              <Link to="/my-questions"><a className="nav-link text-white" href=""> My Questions </a></Link>
            </li>
            <li className="nav-item active">
              <Link to="/student"><a className="nav-link text-white" href=""> Student </a></Link>
            </li>
            <li className="nav-item">
              <Link to="/standings"><a className="nav-link text-white" href=""> Standings </a></Link>
            </li>
          </ul>
          <ul className="pull-right navbar-nav">
            <li className="nav-item">
              <Link to="/profile"><a className="nav-link text-white" href=""> Profile </a></Link>
            </li>
            <li className="nav-item">
              <Link to="/logout"><a className="nav-link text-white" href="#">Logout</a></Link>
            </li>
          </ul>
        </div>
      </nav>

      </div>


      {redirect}
      {redirectQ}
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/my-tests" component={Home}/>
        <Route path="/create-test" component={Test}/>
        <Route path="/create-questions" component={Questions}/>
        <Route path="/my-questions" component={MyQuestions} />
        <Route path="/edit-question/:id" component={EditQuestion}/>
        <Route path="/edit-test/:id" component={EditTest}/>
        <Route path="/choose-questions" component={AddQuestionToTest}/>
        <Route path="/used-in-tests/:id" component={UsedInWhichTests}/>
        <Route path="/student" component={StudentList} />
        <Route path="/standings" component={Standings} />
        <Route path="/profile" component={ProfileView} />
        //logout button is made to simulate login
        <Route path="/logout" component={Select} />
      </Switch>

      </div>
      </div>
    );
  }
}

export default withRouter(App);
